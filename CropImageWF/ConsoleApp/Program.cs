﻿using System;
using System.IO;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var dir = @"C:\Users\stand\Desktop\test_data";
            var dirs = Directory.GetDirectories(dir);
            foreach (var path in dirs)
            {
                var executablePath = path + "\\focus.dat";
                var str = File.ReadAllLines(executablePath);
                for(int i = 0; i < 3; i++)
                {
                    if (str[i][2] == '0')
                    {
                        str[i] = $"{str[i][0]} 2";
                    }
                    else if(str[i][2] == '2')
                    {
                        str[i] = $"{str[i][0]} 0";
                    }
                }
                File.WriteAllText(executablePath, string.Join('\n', str));
            }
            Console.Read();
        }

        
    }
}

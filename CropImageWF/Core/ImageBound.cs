﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ImageBound
    {
        public int Focus { get; set; }
        public int DegreeOfTrust { get; set; }
    }
}

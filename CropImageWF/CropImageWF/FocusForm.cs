﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CropImageWF
{
    public partial class FocusForm : Form
    {
        public List<int> focuses = new List<int>();
        public FocusForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            focuses.Clear();
            focuses.Add(int.Parse(richTextBox1.Text));
            focuses.Add(int.Parse(richTextBox2.Text));
            focuses.Add(int.Parse(richTextBox3.Text));
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}

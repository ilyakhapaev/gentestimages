﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Core;
using Ionic.Zip;

namespace CropImageWF
{
    public partial class Form1 : Form
    {
        private List<Image> imageList = new List<Image>();
        private Random random = new Random();
        private List<int> listFocus = new List<int>();
        private static int clicked = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            imageList.Clear();
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = false;
            folderDlg.SelectedPath = Path.GetDirectoryName(Application.ExecutablePath);
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBox1.Text = folderDlg.SelectedPath;
                pictureBox1.Image = Image.FromFile($"{folderDlg.SelectedPath}\\1.png");
                LoadImages(folderDlg.SelectedPath);
                pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
                var focusDlg = new FocusForm();
                var focusRes = focusDlg.ShowDialog();
                if (focusRes == DialogResult.OK)
                {
                    listFocus.Clear();
                    listFocus.AddRange(focusDlg.focuses);
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null)
            {
                MessageBox.Show("Загрузите изображение");
                return;
            }
            MouseEventArgs me = (MouseEventArgs)e;
            Point coordinates = me.Location;
            if (coordinates.Y + 16 > pictureBox1.Image.Height || coordinates.Y - 16 < 0
                                                              || coordinates.X + 16 > pictureBox1.Width ||
                                                              coordinates.X - 16 < 0)
            {
                MessageBox.Show("Выберите другую точку");
            }
            else
            {
                var images = GetCroppedImages(coordinates);
                //var imagesForm = new ListImagesForm(images);
                //var imagesFormResult = imagesForm.ShowDialog();
                SaveImages(images);
            }
        }

        private void LoadImages(string path)
        {
            for (int i = 0; i < 3; i++)
            {
                imageList.Add(Image.FromFile($"{path}\\{i}.png"));
            }
        }

        private List<Image> GetCroppedImages(Point point)
        {
            var result = new List<Image>();

            var cropArea = new Rectangle(point.X - 16, point.Y - 16, 32, 32);

            foreach (var image in imageList)
            {
                Bitmap bmpImage = new Bitmap(image);
                result.Add(bmpImage.Clone(cropArea, bmpImage.PixelFormat));
            }

            return result;
        }

        private void SaveImages(List<Image> images, List<ImageBound> imageBounds)
        {
            using (ZipFile zip = new ZipFile())
            {
                try
                {
                    for (int i = 0; i < images.Count; i++)
                    {
                        var name = $"{i}.png";
                        images[i].Save(name);
                        zip.AddFile(Path.GetFileName(name));
                    }

                    var pathTxt = "focus.dat";
                    File.WriteAllText(pathTxt, string.Empty);
                    using (StreamWriter sw = new StreamWriter(pathTxt))
                    {
                        for (int i = 0; i < imageBounds.Count; i++)
                        {
                            sw.WriteLine($"{listFocus[i]} {2-imageBounds[i].DegreeOfTrust}");
                        }
                    }
                    
                    zip.AddFile(pathTxt);
                    zip.Save($"{DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss")}.zip");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR:\n\n" + ex.Message);
                    return;
                }

            }
        }

        private void SaveImages(List<Image> images)
        {
            try
            {
                var date = DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss");

                Directory.CreateDirectory($@"{date}");
                Directory.SetCurrentDirectory($@"{date}");
                var cur = Directory.GetCurrentDirectory();
                for (int i = 0; i < images.Count; i++)
                {
                    var name = $"{i}.png";
                    images[i].Save(name);
                }

                var pathTxt = $"focus.dat";
                File.WriteAllText(pathTxt, string.Empty);
                var list = new List<ImageBound>();
                using (StreamWriter sw = new StreamWriter(pathTxt))
                {
                    for (int i = 0; i < 3; i++)
                    {
                        var degreeOfTrust = 0;
                        switch (i)
                        {
                            case 0:
                                degreeOfTrust = random.Next(1, 2);
                                break;
                            case 1:
                                degreeOfTrust = random.Next(0, list[i-1].DegreeOfTrust == 2 ? 2 : list[i - 1].DegreeOfTrust);
                                break;
                            case 2:
                                degreeOfTrust = random.Next(0, list[i-1].DegreeOfTrust == 1 ? 1 : 0);
                                break;
                        }
                        list.Add(new ImageBound
                        {
                            Focus = listFocus[i], DegreeOfTrust = degreeOfTrust
                        });
                        sw.WriteLine($"{i} {degreeOfTrust}");
                    }
                }

                label1.Text = $"{++clicked}";
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR:\n\n" + ex.Message);
                return;
            }
            finally
            {
                Directory.SetCurrentDirectory("..\\");
            }

        }
    }
}

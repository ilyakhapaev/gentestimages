﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core;

namespace CropImageWF
{
    public partial class ListImagesForm : Form
    {
        private List<Image> _images;
        public List<ImageBound> bounds;
        public ListImagesForm(List<Image> images)
        {
            _images = images;
            InitializeComponent();
            pictureBoxImage1.Image = images[0];
            pictureBoxImage2.Image = images[1];
            pictureBoxImage3.Image = images[2];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bounds = new List<ImageBound>();
            bounds.Add(new ImageBound
            {
                Focus = 0,
                DegreeOfTrust = int.Parse(richTextBox2.Text),
            });
            bounds.Add(new ImageBound
            {
                Focus = 1,
                DegreeOfTrust = int.Parse(richTextBox4.Text),
            });
            bounds.Add(new ImageBound
            {
                Focus = 2,
                DegreeOfTrust = int.Parse(richTextBox6.Text),
            });
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
